/*
 * Name: Ethan Miller
 * EID:  eam3422
 * Assignment: JavaCars, assignment number 5
 *
 * Description:
 * the purpose of this example applet it to draw a boxy car out of basic 
 * graphical shapes (rectangles, circles and lines) on the applet window. 
 * It's position and size has been predetermined by sketching it out on graph paper first. 
 * The specific values of the coordinates of the shapes have been arbitrarily chosen. */ 

package assignment5;

import java.util.concurrent.ThreadLocalRandom;
import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics; 
import java.awt.Graphics2D; 
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D; 
import java.awt.geom.Line2D; 
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.*;
import java.awt.geom.GeneralPath;
import java.util.Random;

import javax.swing.JPanel; 

//***********************************************************************//
///////////////////////////////////////////////////////////////////////////
//***********************************************************************//
public class A5Driver extends Applet implements Runnable, ActionListener {
	
	// Fun Graphics - set to "true" for a silly time
	boolean funGraphics = false;
	
	// General definitions
	Car car1, car2, car3, car4, car5;
	int stageHeight = 350;
	int stageWidth  = 900;
	int skyHeight = this.stageHeight/3;
    boolean setButton = false;
    int winPosition;
    
    // Sky definitions
    int skyYdivisions  = skyHeight;
    int[] skyColors	   = new int[skyYdivisions];
    
    // Grass definitions
    int grassXdivisions         = stageWidth;  // multiple of stageWidth
    int grassYdivisions         = stageHeight; // multiple of stageHeight
    int numGreenColor           = 800;
    int numGrassEndPos          = 20;
    int[][] grassColors         = new int[numGreenColor][3]; 
	int[][] grassEndPos         = new int[numGrassEndPos][2];

	int     numGrassRandIndex   = (stageHeight-skyHeight)*stageWidth;
    int[]   grassRandIndexArray = new int[numGrassRandIndex]; 
    
    int[][] grassRandPosArray   = new int[numGrassRandIndex][2];
	
	// Color definitions
	Color skyBlue     = new Color(   59,  185, 255 );
	Color grassGreen  = new Color( 0x52, 0x6F, 0x35);
	
    // Double Buffering definitions
    Dimension bufDimension;
    Image 	  bufImage;
    Graphics  bufGraphics;
    
    // Car tracking definitions
    int winCar;
    int winners;
    StopWatch timer = new StopWatch();
    
	  ///////////////////
	  public void init( )
	  {
	   car1 = new Car(1);
	   car2 = new Car(2);
	   car3 = new Car(3);
	   car4 = new Car(4);
	   car5 = new Car(5);
	
	   Thread t = new Thread(this);
	
	   bufImage	   = createImage( this.stageWidth, this.stageHeight );
	   bufGraphics = bufImage.getGraphics();
	   
	   this.grassArraysInit();
	   
	   t.start();
	  }
	
	 //////////////////////////////// 
	 public void update(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;

		paint(g);
	 }
	 
	 ////////////////////////////////
	 private void grassArraysInit( ) {
		 
		 for( int x = 0; x < numGreenColor; x ++ ) {

			int rLowBound  = 0x00;
			int rHighBound = 0x26;

			int gLowBound  = 0x80;
			int gHighBound = 0xCF;
			
			int bLowBound  = 0x00;
			int bHighBound = 0x32;
			 
			int rRandVal = ThreadLocalRandom.current().nextInt(rLowBound, rHighBound + 1);
			int gRandVal = ThreadLocalRandom.current().nextInt(gLowBound, gHighBound + 1);
			int bRandVal = ThreadLocalRandom.current().nextInt(bLowBound, bHighBound + 1);
			 
			grassColors[x][0] = rRandVal; // r
			grassColors[x][1] = gRandVal; // g
			grassColors[x][2] = bRandVal; // b
		 }
		 
		 int xLowBound    = 0;
		 int xHighBound   = 20;//10;//2;
		 
		 int yLowBound    = 0;
		 int yHighBound   = 20;//10;//4;
		 
		 for( int z = 0; z < numGrassEndPos; z ++ ) {
			 
			 int negXComp     = xHighBound/2;
			 int negYComp     = yHighBound/2;
			 
			 int xEndRandVal = ThreadLocalRandom.current().nextInt(xLowBound, xHighBound + 1) - negXComp;
			 int yEndRandVal = ThreadLocalRandom.current().nextInt(yLowBound, yHighBound + 1) - negYComp;
			 
			 grassEndPos[z][0] = xEndRandVal; // x
			 grassEndPos[z][1] = yEndRandVal; // y
		 }
		 
		 for( int r = 0; r < numGrassRandIndex; r ++ ) {
			 
			 grassRandIndexArray[r] = ThreadLocalRandom.current().nextInt(0, numGreenColor);
		 }
		 
		 for( int p = 0; p < numGrassRandIndex; p ++ ) {
			 
			 grassRandPosArray[p][0] = ThreadLocalRandom.current().nextInt(0, numGrassEndPos);
			 grassRandPosArray[p][1] = ThreadLocalRandom.current().nextInt(0, numGrassEndPos);
		 }
	 }
	 
	 /////////////////////////////
	 public void paint(Graphics g)
	 {  
		// Background colors
		bufGraphics.setColor( skyBlue );
		bufGraphics.fillRect(0, 0, this.stageWidth, skyHeight );
		bufGraphics.setColor( grassGreen );
		bufGraphics.fillRect(0, this.stageHeight, this.stageWidth, this.stageHeight);
		bufGraphics.fillRect(0, skyHeight, this.stageWidth, this.stageHeight - skyHeight);
	
		// Sky Gradient
		int sR = 0x20;
		int sG = 0x52;
		int sB = 0xB9;
		
		for( int y = 0; y <= skyYdivisions; y++ ) {
			
			if( funGraphics == false ) {
				sR++;
				if(y%2 == 0) { sG++; }
				if(y%3 == 0) { sB++; }
			}
			else if( funGraphics == true ) {

				sR += ThreadLocalRandom.current().nextInt(0, 3);
				sG += ThreadLocalRandom.current().nextInt(0, 2);
				sB += ThreadLocalRandom.current().nextInt(0, 1);
			}
				
			Color skyColor = new Color( sR, sG, sB );
			bufGraphics.setColor( skyColor );
			
			int sX1 = 0;
			int sX2 = this.stageWidth;
			
			bufGraphics.drawLine(sX1, y, sX2, y);
		}
		
		// Blades of grass
		int grassCntr        = 0;
		int grassRandCntr    = 0;
		int posCntrX	 	 = 0;
		int posCntrY		 = 0;
		int grassColPick     = 0;
		int grassRandPosCntr = 0;

		for( int y = 1; y <= grassYdivisions; y++ ) {
			for( int x = 1; x <= grassXdivisions; x++ ) {
				
				if( funGraphics == true ) {
					grassCntr = ThreadLocalRandom.current().nextInt(0, numGreenColor);
					posCntrX  = ThreadLocalRandom.current().nextInt(0, numGrassEndPos);
					posCntrY  = ThreadLocalRandom.current().nextInt(0, numGrassEndPos);
				}
				else {
					grassRandCntr++;
					if( grassRandCntr % numGrassRandIndex == 0 ) { grassRandCntr = 0; }
					grassCntr = grassRandIndexArray[grassRandCntr]; 
					
					posCntrX = grassRandPosArray[grassRandCntr][0];
					posCntrY = grassRandPosArray[grassRandCntr][1];
				}
				
				int R = grassColors[grassCntr][0];
				int G = grassColors[grassCntr][1];
				int B = grassColors[grassCntr][2];
				
				Color randGreen = new Color( R, G, B );
				
				bufGraphics.setColor( randGreen );
				
				int gX1 = x*(this.stageWidth/grassXdivisions);
				int gY1 = skyHeight + y*(this.stageHeight-skyHeight)/grassYdivisions;

				int gX2 = gX1 + grassEndPos[posCntrX][0];
				int gY2 = gY1 + grassEndPos[posCntrY][1];
				
				bufGraphics.drawLine(gX1, gY1, gX2, gY2);
			}
		} 
			
		// Checkered finishing line
		int finishLineYstart = skyHeight + 10;
		int finishLineYend   = this.stageHeight;
		int checkerWidth     = (this.stageWidth - winPosition)/2 - 2;
		int checkerYcount    = 18;
		int checkerHeight    = (finishLineYend - finishLineYstart)/checkerYcount;
		bufGraphics.setColor( Color.WHITE );
		bufGraphics.drawLine( winPosition, finishLineYstart, winPosition, finishLineYend);
		bufGraphics.setColor( Color.WHITE );
		
		for( int x = 0; x <= 1; x ++ ) {
			for( int y = 0; y <= checkerYcount; y++ ) {
				
				int xPos;
				if( (y+x)%2==0 ) { bufGraphics.setColor( Color.WHITE ); }
				else         { bufGraphics.setColor( Color.BLACK ); }
				
				if( x == 0 ) { xPos = winPosition;                }
				else         { xPos = winPosition + checkerWidth; }
				
				int yPos = finishLineYstart + y*checkerHeight;
				
				bufGraphics.fillRect( xPos, yPos, checkerWidth, checkerHeight);
			}
		}
			
		car1.paintCar( bufGraphics );
		car2.paintCar( bufGraphics );
		car3.paintCar( bufGraphics );
		car4.paintCar( bufGraphics );
		car5.paintCar( bufGraphics );
		
		bufGraphics.setColor( Color.WHITE );
		
		if( winners > 1 ) {
			int messagePixelWidth = 40*5 + 12; // is exact
			int xBasePos 		  = this.stageWidth/2 - messagePixelWidth/2;
			bufGraphics.drawString( "Congratulations to all the winning cars!", xBasePos, this.stageHeight - 10);
		}
		else if( winners == 1 ) {
			int messagePixelWidth = 22*5 + 10; // is exact
			int xBasePos          = this.stageWidth/2 - messagePixelWidth/2;
			
			switch( winCar ) {
				case 1: bufGraphics.drawString("Congratulations car 1!", xBasePos, this.stageHeight - 10); break;
				case 2: bufGraphics.drawString("Congratulations car 2!", xBasePos, this.stageHeight - 10); break;
				case 3: bufGraphics.drawString("Congratulations car 3!", xBasePos, this.stageHeight - 10); break;
				case 4: bufGraphics.drawString("Congratulations car 4!", xBasePos, this.stageHeight - 10); break;
				case 5: bufGraphics.drawString("Congratulations car 5!", xBasePos, this.stageHeight - 10); break;
			}
		}
		 g.drawImage( bufImage, 0, 0, this );
	 }
	
	  //////////////////
	  public void run( ) {
		  
		  while( true )
		  {
			  
			  try {
					int buttonWidth = 100;
					int buttonHeight = 20;
					int buttonXstart = buttonWidth/2;
					int buttonYstart = buttonHeight/2;
					
					Button rButton = new Button("Restart Race");
					this.setLayout(null);
					rButton.setBounds( buttonXstart, buttonYstart, buttonWidth, buttonHeight ); // top left corner 
					rButton.addActionListener(this);
					this.add(rButton);
					setButton = true;
			  }
			  catch(Exception e){}

			  while( winners == 0 )
			  {
			     try
			     {
			    	 timer.start();
			    	 Random rand   = new Random();
			    	 int stepBound = 2;
			    	 int c1 = rand.nextInt(stepBound);
			    	 int c2 = rand.nextInt(stepBound);
			    	 int c3 = rand.nextInt(stepBound);
			    	 int c4 = rand.nextInt(stepBound);
			    	 int c5 = rand.nextInt(stepBound);
			    	 
			    	 car1.moveCarX( c1 );
			    	 car2.moveCarX( c2 );
			    	 car3.moveCarX( c3 );
			    	 car4.moveCarX( c4 );
			    	 car5.moveCarX( c5 );
					   
			    	 // winning case check
			    	 winPosition = this.stageWidth - 25;
			    	 winCar  = 0;
			    	 winners = 0;
			    	 
			    	 if( car1.getCarXpos() >= winPosition ) { winCar = 1; winners++; }
			    	 if( car2.getCarXpos() >= winPosition ) { winCar = 2; winners++; }
			    	 if( car3.getCarXpos() >= winPosition ) { winCar = 3; winners++; }
			    	 if( car4.getCarXpos() >= winPosition ) { winCar = 4; winners++; }
			    	 if( car5.getCarXpos() >= winPosition ) { winCar = 5; winners++; }
			    	 
			    	 if( winners > 0 ) { 
			    		
			    		timer.stop();
			    	 	System.out.println( "Elapsed racing time: " + String.valueOf( timer.getElapsedTime()) + " milliseconds." );
			    	 }
			    	 
					 Thread.sleep(6);
					 repaint();
			     }
			     
			     catch(Exception e){}
			  }
		  }
	  }
	  
	  ////////////////////////////////////////////
	  public void actionPerformed(ActionEvent e) {
		  
		  timer.stop();
		  
		  car1.resetCarX();
		  car2.resetCarX();
		  car3.resetCarX();
		  car4.resetCarX();
		  car5.resetCarX();
		  
		  if( winners == 0 ) {
			  
			  System.out.println( "Elapsed racing time before reset: " + String.valueOf(timer.getElapsedTime()) + " milliseconds." );
		  }
		  
		  winners = 0;
		  
		  timer.reset();
	  }

}

