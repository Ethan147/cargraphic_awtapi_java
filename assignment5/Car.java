/*
 * Name: Ethan Miller
 * EID:  eam3422
 * Assignment: JavaCars, assignment number 5
 */

package assignment5;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;

/////////
class Car
{
		// color definitions
		Color carNumBadge = new Color( 0xFF, 0xFF, 0xFF);
		Color burntOrange = new Color( 0xBF, 0x57, 0x00);
	    
	    // car Number
	    int carNumber;
	    
	    // car body definitions
	    int carBodyHeight = 15; 
	    int carBodyWidth  = 70; 
	    int carBodyXstart = 60; 
	    int carBodyYstart = 60;
	    int carBodyXinit  = 60;
	    
	    // car identifier definitions
	    String[] carIdentifier   = {"1", "2", "3", "4", "5"};
	    Color[]  carPlaqueColors = { Color.RED, Color.GREEN, Color.WHITE, Color.LIGHT_GRAY, Color.MAGENTA };
	    int plaqueDiameter = 12;
	    int carNumberX     = carBodyXstart + carBodyWidth/2; 
	    int carNumberY     = carBodyYstart + plaqueDiameter/6; 
	    int carNumberXinit = carNumberX;
	    
	    // car wind-shield definitions
	    int carR1x = carBodyXstart + 15; 
	    int carR1y = carBodyYstart;      
	    int carR2x = carR1x + 10;        
	    int carR2y = carR1y - 10;        
	    int carR3x = carR2x + 20;        
	    int carR3y = carR2y;             
	    int carR4x = carR3x + 10;        
	    int carR4y = carR3y + 10;        
	    int carR1Xinit = carR1x;
	    int carR2Xinit = carR2x;
	    int carR3Xinit = carR3x;
	    int carR4Xinit = carR4x;
	    
	    // car tire definitions
	    int tireDiameter       = 10;
	    int carRearTireXstart  = carR1x - tireDiameter/2; //140;
	    int carRearTireYstart  = carR1y + carBodyHeight - tireDiameter/2; //120;
	    int carFrontTireXstart = carR4x - tireDiameter/2; //110;
	    int carFrontTireYstart = carR4y + carBodyHeight - tireDiameter/2; //120;
	    int carRearTireXinit   = carRearTireXstart;
	    int carFrontTireXinit  = carFrontTireXstart;
	
	 public Car( int carNum )
	 {
		 this.carNumber = carNum;
		 int carSpacing = 50;
		 int carOffset  = 50;
		 
		 this.carBodyYstart      += (carNum-1)*carSpacing + carOffset;
		 this.carRearTireYstart  += (carNum-1)*carSpacing + carOffset;
		 this.carFrontTireYstart += (carNum-1)*carSpacing + carOffset;
		 this.carNumberY         += (carNum-1)*carSpacing + carOffset;
		 this.carR1y             += (carNum-1)*carSpacing + carOffset;
		 this.carR2y             += (carNum-1)*carSpacing + carOffset;
		 this.carR3y             += (carNum-1)*carSpacing + carOffset;
		 this.carR4y             += (carNum-1)*carSpacing + carOffset;
	 }
	 
	 public int getCarXpos() {
		 return (this.carBodyXstart + this.carBodyWidth);
	 }
	 
	 public void resetCarX( ) {
		 
		 this.carBodyXstart      = this.carBodyXinit;
         this.carRearTireXstart  = this.carRearTireXinit;
         this.carFrontTireXstart = this.carFrontTireXinit;
         this.carNumberX         = this.carNumberXinit;
         this.carR1x             = this.carR1Xinit;
         this.carR2x             = this.carR2Xinit;
         this.carR3x             = this.carR3Xinit;
         this.carR4x             = this.carR4Xinit;
	 }
	 
	 public void moveCarX( int x ) {
	        
        this.carBodyXstart      += x;
        this.carRearTireXstart  += x;
        this.carFrontTireXstart += x;
        this.carNumberX         += x; 
        this.carR1x             += x;
        this.carR2x             += x;
        this.carR3x             += x;
        this.carR4x             += x;
	 }
	    
	 public void moveCarY( int y ) {
		    
		this.carBodyYstart      += y;
		this.carRearTireYstart  += y;
		this.carFrontTireYstart += y;
		this.carNumberY         += y; 
		this.carR1y             += y;
		this.carR2y             += y;
		this.carR3y             += y;
		this.carR4y             += y;
	 }
	 
	 public void paintCar(Graphics g) { 
		Graphics2D g2 = (Graphics2D)g; 
		
		// create the car body 
		GeneralPath body = new GeneralPath();
		body.moveTo( carBodyXstart,                carBodyYstart                 );
		body.lineTo( carBodyXstart + carBodyWidth, carBodyYstart                 );
		body.lineTo( carBodyXstart + carBodyWidth, carBodyYstart + carBodyHeight );
		body.lineTo( carBodyXstart, 			   carBodyYstart + carBodyHeight );
		body.closePath();
		g2.setColor( burntOrange );
		g2.fill( body );
		g2.setColor( Color.BLACK );
	
		// create the car tires 
		Ellipse2D.Double frontTire = new Ellipse2D.Double( carFrontTireXstart, carFrontTireYstart, tireDiameter, tireDiameter); 
		Ellipse2D.Double rearTire  = new Ellipse2D.Double( carRearTireXstart,  carRearTireYstart,  tireDiameter, tireDiameter); 
		g2.setColor( Color.BLACK );
		g2.fill    ( frontTire   );
		g2.fill    ( rearTire    );

		// create car identifying number
		g2.setColor( carPlaqueColors[carNumber-1] );
		Ellipse2D.Double numberPlaque = new Ellipse2D.Double( carNumberX, carNumberY, plaqueDiameter, plaqueDiameter);
		g2.fill( numberPlaque );
		g2.setColor( Color.BLACK );
		g2.drawString( carIdentifier[carNumber-1], carNumberX + 2, carNumberY + 11 );
		
		// create the roof baseline
		GeneralPath roof = new GeneralPath();
		roof.moveTo( carR1x, carR1y );
		roof.lineTo( carR2x, carR2y );
		roof.lineTo( carR3x, carR3y );
		roof.lineTo( carR4x, carR4y );
		roof.closePath();
		
		// create the colored roof section
		GeneralPath roofColor = new GeneralPath();
		roofColor.moveTo( carR1x, carR1y );
		roofColor.lineTo( carR2x, carR2y );
		roofColor.lineTo( carR3x, carR3y );
		roofColor.lineTo( carR3x + (carR4x-carR3x)*0.2,      carR3y + (carR4y-carR3y)*0.2 );
		roofColor.lineTo( carR3x + (carR4x-carR3x)*0.2 - 10, carR3y + (carR4y-carR3y)*0.2 );
		roofColor.lineTo( carR3x + (carR4x-carR3x)*0.2 - 10, carR4y );
		roofColor.closePath();
		g2.setColor( burntOrange );
		g2.fill( roofColor );
		
		// draw all of the car parts on the screen 
		g2.setColor( Color.BLACK );
		g2.draw(body);
		g2.draw(roof);
		g2.draw(roofColor);
		g2.draw(frontTire); 
		g2.draw(rearTire);
	 } 

}
